FROM adoptopenjdk/openjdk16:ubi-minimal-jre
COPY target/annonces-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "annonces-0.0.1-SNAPSHOT.jar"]

package nc.cafat.demo.annonces.services;

import nc.cafat.demo.annonces.model.Annonce;
import nc.cafat.demo.annonces.repositories.AnnonceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnnonceService {

    private final AnnonceRepository annonceRepository;

    public AnnonceService(AnnonceRepository annonceRepository) {
        this.annonceRepository = annonceRepository;
    }

    public List<Annonce> recupererToutesLesAnnonces(){
        return this.annonceRepository.findAll();
    }

    public List<Annonce> recupererToutesLesAnnoncesParCategorie(String category){
        return this.annonceRepository.findByCategory(category);
    }

    public List<Annonce> recupererToutesLesAnnoncesParPrix(int price){
        return this.annonceRepository.findByPrice(price);
    }

    public List<Annonce> recupererToutesLesAnnoncesParDate(String date){
        return this.annonceRepository.findByDate(date);
    }

    public List<Annonce> recupererToutesLesAnnoncesParCategorieEtPrix(String category, int prix){
        return this.annonceRepository.findByCategoryAndPrice(category, prix);
    }

    public List<Annonce> recupererToutesLesAnnoncesParCategorieEtDate(String category, String date){
        return this.annonceRepository.findByCategoryAndDate(category, date);
    }

    public List<Annonce> recupererToutesLesAnnoncesParCategorieEtPrixEtDate(String category, int prix, String date){
        return this.annonceRepository.findByCategoryAndPriceAndDate(category, prix, date);
    }

    public List<Annonce> recupererToutesLesAnnoncesParPrixEtDate(int prix, String date){
        return this.annonceRepository.findByPriceAndDate(prix, date);
    }

    public Annonce creerAnnonce(Annonce annonce){
        return this.annonceRepository.insert(annonce);
    }

    public Annonce updateAnnonce(Annonce annonce){
        return this.annonceRepository.save(annonce);
    }
}

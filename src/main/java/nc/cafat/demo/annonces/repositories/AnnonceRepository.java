package nc.cafat.demo.annonces.repositories;

import nc.cafat.demo.annonces.model.Annonce;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnonceRepository extends MongoRepository<Annonce, String> {

    List<Annonce> findByCategory(String category);

    List<Annonce> findByPrice(int price);

    List<Annonce> findByDate(String date);

    List<Annonce> findByCategoryAndPrice(String category, int price);

    List<Annonce> findByCategoryAndDate(String category, String date);

    List<Annonce> findByCategoryAndPriceAndDate(String category, int price, String date);

    List<Annonce> findByPriceAndDate(int price, String date);
}

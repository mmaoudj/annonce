package nc.cafat.demo.annonces.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class Seller {
    public String lastname;
    @Field("fistname")
    public String firstname;
    public String city;
    public String phone;
    public String email;
    public String gender;
}

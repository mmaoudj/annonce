package nc.cafat.demo.annonces.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Annonce {

    @Id
    public String id;
    public String title;
    public String date;
    public String description;
    public Seller seller;
    public int price;
    public String photoURL;
    public String category;
}

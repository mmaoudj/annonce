package nc.cafat.demo.annonces.modeldto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AnnonceDto implements Serializable {

    public String title;
    public String date;
    public String description;
    public SellerDto seller;
    public int price;
    public String photoURL;
    public String category;
}

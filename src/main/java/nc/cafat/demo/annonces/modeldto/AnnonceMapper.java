package nc.cafat.demo.annonces.modeldto;

import nc.cafat.demo.annonces.model.Annonce;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AnnonceMapper {

    AnnonceMapper INSTANCE = Mappers.getMapper(AnnonceMapper.class);

    public Annonce annonceDtoToAnnonce(AnnonceDto annonceDto);
}

package nc.cafat.demo.annonces.controller;

import nc.cafat.demo.annonces.model.Annonce;
import nc.cafat.demo.annonces.modeldto.AnnonceDto;
import nc.cafat.demo.annonces.modeldto.AnnonceMapper;
import nc.cafat.demo.annonces.services.AnnonceService;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
public class AnnoncesController {

    private final AnnonceService annonceService;

    public AnnoncesController(AnnonceService annonceService) {
        this.annonceService = annonceService;
    }

    @PostMapping(path = "/annonces")
    public Annonce enregistrerAnnonce(AnnonceDto annonce){
        return this.annonceService.creerAnnonce(AnnonceMapper.INSTANCE.annonceDtoToAnnonce(annonce));
    }

    @PutMapping(path="/annonces/{id}")
    public Annonce miseAJourAnnonce(@PathParam("id") String id, AnnonceDto annonce){
        Annonce annonceBDD = AnnonceMapper.INSTANCE.annonceDtoToAnnonce(annonce);
        annonceBDD.setId(id);
        return this.annonceService.updateAnnonce(annonceBDD);
    }

    @GetMapping("/annonces")
    public List<Annonce> recupererToutesLesAnnonces(
            @RequestParam(required = false, name = "categorie") String categorie,
            @RequestParam(required = false, name = "prix") Integer prix,
            @RequestParam(required = false, name = "date") String date
    ){
        if(categorie != null){
            if(prix != null) {
                if(date != null){
                    // Catégorie, Prix et Date renseignés
                    return this.annonceService.recupererToutesLesAnnoncesParCategorieEtPrixEtDate(categorie, prix, date);
                } else {
                    // Catégorie, Prix renseignés
                    return this.annonceService.recupererToutesLesAnnoncesParCategorieEtPrix(categorie, prix);
                }
            } else {
                if(date != null){
                    // Catégorie, Date renseignés
                    return this.annonceService.recupererToutesLesAnnoncesParCategorieEtDate(categorie, date);
                } else {
                    // seule la catégorie est renseignée
                    return this.annonceService.recupererToutesLesAnnoncesParCategorie(categorie);
                }
            }
        } else {
            if(prix != null) {
                if(date != null){
                    // seule le prix est renseigné
                    return this.annonceService.recupererToutesLesAnnoncesParPrixEtDate(prix, date);
                } else {
                    // seul le prix est renseigné
                    return this.annonceService.recupererToutesLesAnnoncesParPrix(prix);
                }
            } else {
                if(date != null){
                    // seule la date est renseignée
                    return this.annonceService.recupererToutesLesAnnoncesParDate(date);
                } else {
                    // aucun paramètre renseigné
                    return this.annonceService.recupererToutesLesAnnonces();
                }
            }
        }

    }
}
